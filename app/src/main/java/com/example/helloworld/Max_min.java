package com.example.helloworld;
    /**
     * Implementation of the class maximum and minimum
     * @author Enokyan Vage IP-812
     * @version 1.0
     * */
public class Max_min {

    /**
     * Maximum value function
     * @param x first variable
     * @param y second variable
     * @return returns the maximum value
     */
    public int max(int x, int y)
    {
        if(x < y)
        {
            return y;
        }
        else return x;
    }
    /**
     * Function of getting the minimum value
     * @param x first variable
     * @param y second variable
     * @return returns the minimum value
     */
    public int min(int x, int y)
    {
        if(x < y)
        {
            return x;
        }
        else return y;
    }
}
