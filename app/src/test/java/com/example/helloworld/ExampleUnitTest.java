package com.example.helloworld;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        Max_min m = new Max_min();
        assertEquals(4,2+2);

        assertEquals(m.max(1,5),5);
        assertEquals(m.max(2,3),3);
        assertEquals(m.max(7,3),7);
        assertEquals(m.max(4,1),4);

        assertEquals(m.min(1,5),1);
        assertEquals(m.min(2,3),2);
        assertEquals(m.min(7,3),3);
        assertEquals(m.min(4,1),1);
    }
}